# Raport z testów użytkowych

## 1. Wysyłanie wiadomości

Informacje o chacie wykorzystanym do testów:
```
{
    "id": 3,
    "name": "Sebastian i Anna",
    "type": "personal",
    "adminUsername": null,
    "membersUsername": [
        "sebastian",
        "anna"
    ]
}
```

___

Po wykonaniu:
* POST `localhost:8080/api/chats/3`
    ```
    {
        "chatId": 3,
        "sender": "sebastian",
        "type": "text",
        "body": "Ciemność widzę!"
    }
    ```
    Wiadmość zasotaje wysłana i zapisana w bazie po wprowadzeniu powyższego zestawu danych - **pożądane** zachowanie.

___

* POST `localhost:8080/api/chats/3`
    ```
    {
        "chatId": 3,
        "sender": "laska",
        "type": "text",
        "body": "Dodaj gazu, tylko powli."
    }
    ```
    Wiadmość zasotaje wysłana i zapisana w bazie po wprowadzeniu powyższego zestawu danych - **niepożądane** zachowanie, użytkownik `laska` nie jest członkiem czatu. Wiadmość nie powinna zostać wysłana.

___

* POST `localhost:8080/api/chats/3`
    ```
    {
        "chatId": 3,
        "sender": "sebastian",
        "type": "text",
        "body": ""
    }
    ```
    Serwer zwraca `400 Bad Request` - zawartość waidomości nie powinna być pusta. **Pożądane** zachowanie.

___

* POST `localhost:8080/api/chats/3`
    ```
    {
        "chatId": 3,
        "sender": "sebastian",
        "type": "",
        "body": "Dodaj gazu, tylko powli."
    }
    ```
    Serwer zwraca `400 Bad Request` - typ wiadomości nie powinnien być pusty. **Pożądane** zachowanie.

___

* POST `localhost:8080/api/chats/3`
    ```
    {
	    "chatId": 3,
	    "sender": "",
	    "type": "text",
	    "body": "Dodaj gazu, tylko powli."
    }
    ```
    Serwer zwraca `400 Bad Request` - nadawca wiadomości nie powinnien być pusty. **Pożądane** zachowanie.

___

* POST `localhost:8080/api/chats/3`
    ```
    {
	    "sender": "sebastian",
	    "type": "text",
	    "body": "Dodaj gazu, tylko powli."
    }
    ```
    Serwer zwraca `400 Bad Request` - nie udaje się zdeserializować danych, w tym przypadku brak `chatId`, ale brak którejkolwiek z danych powoduje indentyczne zachowanie. **Pożądane** zachowanie.

___

* POST `localhost:8080/api/chats/3`
    ```
    {
	    "chatId": 2,
	    "sender": "sebastian",
	    "type": "text",
	    "body": "Dodaj gazu, tylko powli."
    }
    ```
    Wiadmość zasotaje wysłana i zapisana w bazie po wprowadzeniu powyższego zestawu danych - **niepożądane** zachowanie, ponieważ `chatId` ze ścieżki różni się od `chatId` z ciała zapytania. W takiej sytuacji wartość ze ścieżki powinna być nadrzędna i zostać podmieniona przed zapisem do bazy. (błąd architektoniczny)

## 2. Odbieranie wiadomości

Informacje o chacie wykorzystanym do testów:
```
{
    "id": 3,
    "name": "Sebastian i Anna",
    "type": "personal",
    "adminUsername": null,
    "membersUsername": [
        "sebastian",
        "anna"
    ]
}
```

___

Po wykonaniu:
* GET `localhost:8080/api/chats/3/messages`

    Poprawna odpowiedź - **pożądane zachowanie**
___

* GET `localhost:8080/api/chats/1337/messages`

    Serwer zwaraca `200 OK` - **niepożądane zachowanie**. Powinien zwrócić `404 Not Found`, bo czat o id `1337` nie istnieje.

## 3. Tworzenie czatów

*  POST `localhost:8080/api/chats`
    ```
    {
        "name": "Jan i Krystyna",
        "type": "personal",
        "adminUsername": null,
        "membersUsername": ["jan", "krystyna"]
    }
    ```
    Pomyślne utworzenie czatu w sytuacji, gdy typ czatu to `personal` i czat z takimi użytkownikami jeszcze nie istnieje - **pożądane zachowanie**

___

*  POST `localhost:8080/api/chats`
    ```
    {
        "name": "Jan i Krystyna",
        "type": "personal",
        "adminUsername": null,
        "membersUsername": ["jan", "krystyna"]
    }
    ```
    Pomyślne utworzenie czatu w sytuacji, gdy typ czatu to `personal` i czat z takimi użytkownikami już istnieje - **niepożądane zachowanie**. Czat pesonalny dla danych dwóch użytkowników powinien być utworzony tylko raz.

___

* POST `localhost:8080/api/chats`
    ```
    {
        "name": "Jan, Krystyna i Adrian",
        "type": "personal",
        "adminUsername": null,
        "membersUsername": ["jan", "krystyna", "adrian"]
    }
    ```
    Pomyślne utworzenie czatu typu `personal` w przypadku podania więcej niż 2 użytkowników - **niepożądane zachowanie**. Typ `personal` powinien być zarezerwowany tylko dla czatów z 2 użytkownikami.

___

* POST `localhost:8080/api/chats`
    ```
    {
        "name": "Jan",
        "type": "self",
        "adminUsername": null,
        "membersUsername": ["jan"]
    }
    ```
    Pomyślne utworzenie czatu z tylko jednym użytkownikiem - **pożądane zachowanie**.

___

* POST `localhost:8080/api/chats`
    ```
    {
        "name": "Jan",
        "type": "personal",
        "adminUsername": null,
        "membersUsername": ["jan"]
    }
    ```
    Pomyślne utworzenie czatu typu `personal` z tylko jednym użytkownikiem - **niepożądane zachowanie**. Typ `personal` jest zerezerwowany dla czatów tylko z 2 użytkownikami.

___

* POST `localhost:8080/api/chats`
    ```
    {
        "name": "",
        "type": "personal",
        "adminUsername": null,
        "membersUsername": ["jan"]
    }
    ```
    Serwer zwraca `400 Bad Request` - nazwa, typ i lista członków nie może być pusta. **Niepożądane zachowanie**

___

* POST `localhost:8080/api/chats`
    ```
    {
        "name": "Jan",
        "type": "personal",
        "adminUsername": 48,
        "membersUsername": ["jan"]
    }
    ```
    Pomyślne utwozenie czat - **niepożądane zachowanie**. Wartości liczbowe nie powinny być traktowane jako string. Serwer powinien odpowiadać `400 Bad Request`.

___

* POST `localhost:8080/api/chats`
    ```
    {
        "name": "Jan",
        "adminUsername": null,
        "membersUsername": ["jan"]
    }
    ```
    Serwer odpowiada `400 Bad Request`, bo brakuje typu czatu. W przypadku braku któregokolwiek z parametrów serwer zachowuje się tak samo - **działanie pożądane**

## 4. Informacje o czatach

* GET `localhost:8080/api/chats`

    Zwraca listę utworzonych czatów. W przypadku, gdy nie ma żadnych czatóœ, zwróci listę pustą. **Pożądane zachowanie**

___

* GET `localhost:8080/api/chats/2`

    Poprawnie zwraca informacje o czacie, któy istnieje - **zachowanie pożądane**.

___

* GET `localhost:8080/api/chats/1337`
    
    Serwer w ciele odowiedzi zwraca `null` - **niepożądane zachowanie**. Serwer powinien odpowiedzieć `404 Not Found`.