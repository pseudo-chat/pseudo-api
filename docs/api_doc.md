# Dokumentacja API HTTP

**API obsługuje `Chat`, `ChatMessage` i `GroupEvent` operując na danych w fromacie _JSON_.**

Zapytania GET oraz POST dalej skrótowano nazywane są **`GET`** oraz **`POST`**. 


## Legenda

* `{host}` - adres ip hosta (nazwa domeny) wraz z portem, np. `localhost:8080`
* `{chatId}` - id czatu, który nas interesuje


## Obsługa klasy `Chat`

1. Lista czatów `Iterable<Chat>`, które kiedykolwiek otworzył użytkownik wykonana **`GET`** do: 
    *  `{host}/api/chats`

2. Pobranie informacji o konkretnym chacie typu `Optional<Chat>` wykonywane **`GET`** do:
    * `{host}/api/chats/{chatId}`

3. Stworzenie nowego czatu realizowane za pomocą **`POST`** do:
    * `{host}/api/chats`  

        Przykładowe ciało zapytania tworzącego czat
        ```
        {
	        "name": "Adam i Tomek",
	        "type": "personal",
	        "adminUsername": null,
	        "membersUsername": ["adam", "tomek"]
        }
        ```
        Czaty o liczbie uczestników większej niż 2 powinny mieć `"type": "group"`.
    

## Obsługa klasy `ChatMessage`

1. Pobranie listy wiadomości `Iterable<ChatMessage>` z konkretnego czatu można uzyskać poprzez **`GET`** do: 
    * `{host}/{chatId}/messages`  

2. Aby zapisać wiadomość należy wykonać zapytanie **`POST`** zawierające dane wiadomości do:
    * `{host}/{chatId}`
    
        Przykładowe ciało zapytania
        ```
        {
	        "chatId": 1,
	        "sender": "adam",
            "type": "text",
            "body": "Witaj kolego!"
        }
        ```
        

## Obsługa klasy `GroupEvent`

1. Pobranie listy wszystkich wydarzeń `Iterable<GroupEvent>`, które zostały stworzone w danym czacie, można uzyskać porzpez 
**`GET`** do:  
    * `{host}/{chatId}/events`  
      
2. Uzyskiwanie szczegółowych informacji o konkretnym wydarzeniu `Optional<GroupEvent>` realizowane jest za pomocą **`GET`** do:
    * `{host}/{chatId}/events/{eventId}`

3. Stworzenie nowego wydarzenia grupowego realizowane **`POST`** do:
    * `{host}/{chatId}/events`

        Przykładowe ciało zapytanie
        ```
        {
	        "chatId": 2,
    	    "dateTime": "2012-03-19T07:22Z",
            "eventAttenders": ["tom","marek", "asia", "kasia"],
    	    "location": "Działka Tomka",
    	    "name": "Przyjęcie urodzinowe"
        }
        ```