### Simple chat API using Spring Boot.

Run database with Docker:
```
docker run --name **container_name** -e MYSQL_DATABASE=dykow_chat -e  MYSQL_ROOT_PASSWORD=1234 -d mysql
```

Get the IP address of the docker container (it may change every restart of container)
```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' **container_name**
```
