package com.chat_api.pseudo_chat.controller;

import com.chat_api.pseudo_chat.model.GroupEvent;
import com.chat_api.pseudo_chat.repository.ChatRepository;
import com.chat_api.pseudo_chat.repository.GroupEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("api/chats")
public class GroupEventController {

    private final ChatRepository chatRepository;
    private final GroupEventRepository eventRepository;

    @Autowired
    public GroupEventController(ChatRepository chatRepository, GroupEventRepository eventRepository) {
        this.chatRepository = chatRepository;
        this.eventRepository = eventRepository;
    }

    /**
     * GET list of all GroupEvents in given Chat
     * @param chatId id of a {@code Chat}
     * @return list of GroupEvent serialized to JSON
     */
    @GetMapping("{chatId}/events")
    public Iterable<GroupEvent> getEvents(@PathVariable Long chatId) {
        return eventRepository.findByChatId(chatId);
    }

    /**
     * GET details of specific event
     * @param eventId id of a {@code GroupEvent}
     * @return NULL or event details serialized to JSON
     */
    @GetMapping("{chatId}/events/{eventId}")
    public Optional<GroupEvent> getEvent(@PathVariable Long eventId) {
        return eventRepository.findById(eventId);
    }

    /**
     * POST new GroupEvent
     * @param chatId id of event's {@code Chat}
     * @param groupEvent JSON serialized
     */
    @PostMapping("{chatId}/events")
    public String postEvent(@PathVariable Long chatId,
                          @RequestBody @Valid GroupEvent groupEvent) {
        if (chatRepository.findById(chatId).isPresent()) {
            groupEvent.setChatId(chatId);
            eventRepository.save(groupEvent);
            return "Created";
        } else {
            return "Corrupted data!";
        }
    }
}