package com.chat_api.pseudo_chat.controller;

import com.chat_api.pseudo_chat.model.ChatMessage;
import com.chat_api.pseudo_chat.repository.ChatMessageRepository;
import com.chat_api.pseudo_chat.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "api/chats")
public class ChatMessageController {

    private final ChatRepository chatRepository;
    private final ChatMessageRepository chatMessageRepository;

    @Autowired
    public ChatMessageController(ChatRepository chatRepository, ChatMessageRepository chatMessageRepository) {
        this.chatRepository = chatRepository;
        this.chatMessageRepository = chatMessageRepository;
    }

    /**
     * GET list of ChatMessages
     * @param chatId id of a {@code Chat}
     * @return serialized to JSON
     */
    @GetMapping(path = "{chatId}/messages")
    public Iterable<ChatMessage> getMessages(@PathVariable Long chatId) {
        return chatMessageRepository.findByChatId(chatId);
    }

    // TODO check is POSTed ChatId exists
    /**
     * POST ChatMessage
     * @param chatId id of a {@code Chat}
     * @param chatMessage JSON serialized object
     */
    @PostMapping(path = "{chatId}")
    public void postMessage(@PathVariable Long chatId, @RequestBody @Valid ChatMessage chatMessage) {
        chatMessageRepository.save(chatMessage);
    }

    // TODO add getPreviousTenMessagesSince("/{chatId}", date)
    // TODO get unclaimed ChatMessages
    // TODO get by message type
}