package com.chat_api.pseudo_chat.controller;

import com.chat_api.pseudo_chat.model.Chat;
import com.chat_api.pseudo_chat.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("api/chats")
public class ChatController {

    private final ChatRepository chatRepository;

    @Autowired
    public ChatController(ChatRepository chatRepository) {
        this.chatRepository = chatRepository;
    }
    /**
     * GET list of created(accessed) {@code Chat}s
     * @return serialized to JSON
     */
    @GetMapping
    public Iterable<Chat> accessedChats() {
        return chatRepository.findAll();
    }

    /**
     * GET info about given Chat
     * @param chatId id of a {@code Chat}
     * @return JSON serialized object
     */
    @GetMapping("{chatId}")
    public Optional<Chat> chatInfo(@PathVariable Long chatId) {
        return chatRepository.findById(chatId);
    }

    /**
     * POST Chat
     * @param chat JSON serialized object
     */
    @PostMapping
    public void createChat(@RequestBody @Valid Chat chat) {
        chatRepository.save(chat);
    }

    // TODO Put Chat
}