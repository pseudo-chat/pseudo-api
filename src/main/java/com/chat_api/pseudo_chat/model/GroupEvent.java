package com.chat_api.pseudo_chat.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Data
@Entity
public class GroupEvent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private  Long chatId;

    @NotNull
    private Date dateTime;

    @ElementCollection
    private Set<String> eventAttenders;

    @NotEmpty
    private String location;

    @NotEmpty
    private String name;

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }
}