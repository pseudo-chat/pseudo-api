package com.chat_api.pseudo_chat.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@Entity
public class Chat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String type;

    private String adminUsername;

    @ElementCollection
    private Set<String> membersUsername;

    public Chat() {}

    public Chat(@NotBlank String name,
                @NotBlank String type,
                String adminUsername,
                @NotBlank Set<String> membersUsername) {
        this.name = name;
        this.type = type;
        this.adminUsername = adminUsername;
        this.membersUsername = membersUsername;
    }
}
