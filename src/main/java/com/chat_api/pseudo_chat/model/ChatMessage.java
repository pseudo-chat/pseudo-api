package com.chat_api.pseudo_chat.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity
public class ChatMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private Long chatId;

    @NotBlank
    private String sender;

    @NotBlank
    private String type;

    @NotBlank
    private String body;

    private Date dateTime;

    @PrePersist
    protected void onCreate() {
        dateTime = new Date();
    }

    public ChatMessage() {}
//
//    public void setId(Long id) {
//        this.id = id;
//    }
}