package com.chat_api.pseudo_chat.repository;

import com.chat_api.pseudo_chat.model.ChatMessage;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ChatMessageRepository extends CrudRepository<ChatMessage, Long> {
    List<ChatMessage> findByChatId(Long chatId);
    List<ChatMessage> findByType(String type);
}