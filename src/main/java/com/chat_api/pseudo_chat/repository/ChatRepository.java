package com.chat_api.pseudo_chat.repository;

import com.chat_api.pseudo_chat.model.Chat;
import org.springframework.data.repository.CrudRepository;

public interface ChatRepository extends CrudRepository<Chat, Long> {
}