package com.chat_api.pseudo_chat.repository;

import com.chat_api.pseudo_chat.model.GroupEvent;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GroupEventRepository extends CrudRepository<GroupEvent, Long> {
    List<GroupEvent> findByChatId(Long chatId);
    List<GroupEvent> findByName(String name);
}