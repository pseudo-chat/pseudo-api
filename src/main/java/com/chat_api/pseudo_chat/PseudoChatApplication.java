package com.chat_api.pseudo_chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PseudoChatApplication {

	public static void main(String[] args) {
		SpringApplication.run(PseudoChatApplication.class, args);
	}

}
